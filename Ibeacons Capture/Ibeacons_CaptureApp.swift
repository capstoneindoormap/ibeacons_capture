//
//  Ibeacons_CaptureApp.swift
//  Ibeacons Capture
//
//  Created by 荆海洋 on 2021-07-21.
//

import SwiftUI
import Firebase

@main
struct Ibeacons_CaptureApp: App {
    
    init() {
     FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
