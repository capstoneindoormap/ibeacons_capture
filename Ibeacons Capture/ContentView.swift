//
//  ContentView.swift
//  Ibeacons Capture
//
//  Created by 荆海洋 on 2021-07-21.
//

import SwiftUI
import Firebase

struct ContentView: View {
    @State var x = ""
    @State var y = ""
    @State var zoneVal = ""
    @State var pn = ""
    @State var n = 0
    @ObservedObject var dbVM = DbManager()
    @ObservedObject var mVM = MainVM()
   
    var body: some View {
       
        VStack(alignment:.center, spacing:50 ){
            
            HStack(spacing:20){
                Text("81: "+String(self.mVM.ib1))
                Text("186: "+String(self.mVM.ib2))
                Text("84: "+String(self.mVM.ib3))
                Text("185: "+String(self.mVM.ib4))
                Text("174: "+String(self.mVM.ib5))
              
            }
            HStack(spacing:20){
                Text("194: "+String(self.mVM.ib6))
                Text("61: "+String(self.mVM.ib7))
                Text("173: "+String(self.mVM.ib8))
                Text("176: "+String(self.mVM.ib9))
                Text("184: "+String(self.mVM.ib10))
              
            }
            
     
         
            Text(self.mVM.numTest)
            TextField("Place Name", text: self.$pn)
            TextField("Zone",text: self.$zoneVal)
            HStack(spacing:10){
                TextField("x", text: self.$x).keyboardType(.numbersAndPunctuation)
                TextField("y", text: self.$y).keyboardType(.numbersAndPunctuation)
                
            }
            Button("collect"){
              
             
                self.mVM.callScan()
               
              
                
            }
       

            Button("Reset"){
                self.mVM.resetAll()
                self.x=""
                self.y=""
                self.zoneVal = ""
                
            }
        
            Button("Record"){
                if(x != "" && y != ""){
                    let obj:Area = Area(id:"", psX: Double(x) ?? 0, psY: Double(y) ?? 0, rssi: self.mVM.rssiList,floor: 1, placeName: pn, zone:zoneVal)
                        
                    
                    self.dbVM.createArea(areaobj: obj) { docId, error in
                        if(error != ""){
                            print(error)
                        }
                    }
                }else{
                    
                }
            }
    }
}
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
