//
//  MainVM.swift
//  Ibeacons Capture
//
//  Created by 荆海洋 on 2021-07-25.
//

import Foundation
import CoreLocation

class MainVM:NSObject, ObservableObject, CLLocationManagerDelegate{
    var LM: CLLocationManager!
    var bDict : [Int: [Double]] = [:]
    @Published var rssiList = [Int: Double]()
    @Published var numTest = "not ready"
    @Published var disArr:[Double]=[]
    @Published var ib1=0.0
    @Published var ib2=0.0
    @Published var ib3=0.0
    @Published var ib4=0.0
    @Published var ib5=0.0
    @Published var ib6=0.0
    @Published var ib7=0.0
    @Published var ib8=0.0
    @Published var ib9=0.0
    @Published var ib10=0.0
    override init() {
        super.init()

        LM = CLLocationManager()
        LM.delegate = self
        LM.requestAlwaysAuthorization()
    }
    
    func startScanning(){
        let uuid = UUID(uuidString: "00000000-0000-0000-0000-000000000001")!
        let beaconRegion = CLBeaconRegion(uuid: uuid, identifier: "57292")
        
        
        LM.startMonitoring(for: beaconRegion)
        LM.startRangingBeacons(satisfying: beaconRegion.beaconIdentityConstraint)
        
    }
    func callScan(){
        let uuid = UUID(uuidString: "00000000-0000-0000-0000-000000000001")!
        let beaconRegion = CLBeaconRegion(uuid: uuid, identifier: "57292")
        LM.startMonitoring(for: beaconRegion)
        LM.startRangingBeacons(satisfying: beaconRegion.beaconIdentityConstraint)
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        if status == .authorizedAlways{
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self){
                if CLLocationManager.isRangingAvailable(){
                    
                    startScanning()
                    
                    
                }
            }
        }
    }
    
    public func resetAll(){
        rssiList.removeAll()
        numTest = "not ready"
        bDict.removeAll()
        ib1=0
        ib2=0
        ib3=0
        ib4=0
        ib5=0
        ib6=0
        ib7=0
        ib8=0
        ib9=0
        ib10=0
        
    }
    func standardDeviation(arr : [Double]) -> Double
    {
        let length = Double(arr.count)
        let avg = arr.reduce(0, {$0 + $1}) / length
        let sumOfSquaredAvgDiff = arr.map { pow($0 - avg, 2.0)}.reduce(0, {$0 + $1})
        return sqrt(sumOfSquaredAvgDiff / length)
    }
    
    func kalmanFilter(inputDict:[Int: [Double]],initVariance: Double, noise: Double, matchDict:[Int:Double])->[Int : [Double]]{
        //var kalOutput=[Int:Double]()
        var kalOutputDict=[Int:[Double]]()
       // var targetOutput=[Int:Double]()

        for k in inputDict.keys{
            let inputValues = inputDict[k]
            var kalmanGain = 0.0
            let processNoise = noise
            var varianceValue = initVariance
            let measureNoise = standardDeviation(arr: inputValues!)
            var mean = inputValues![0]
            var filtered_list:[Double]=[]
            for value in inputValues! {
                varianceValue = varianceValue+processNoise
                kalmanGain=varianceValue/(varianceValue+measureNoise)
                mean=mean+kalmanGain*(value-mean)
                varianceValue = varianceValue-(kalmanGain*varianceValue)
                filtered_list.append(round(mean))

            }
            kalOutputDict[k]=filtered_list



        }


        return kalOutputDict

    }
//    func kalmanFilter(inputDict:[Int: [Double]],initVariance: Double, noise: Double, matchDict:[Int:Double])->[Int : Double]{
//        //var kalOutput=[Int:Double]()
//        var kalOutputDict=[Int:Double]()
//        var targetOutput=[Int:Double]()
//
//        for k in inputDict.keys{
//            let inputValues = inputDict[k]
//            var kalmanGain = 0.0
//            let processNoise = noise
//            var varianceValue = initVariance
//            let measureNoise = standardDeviation(arr: inputValues!)
//            var mean = inputValues![0]
//            var filtered_list:[Double]=[]
//            for value in inputValues! {
//                varianceValue = varianceValue+processNoise
//                kalmanGain=varianceValue/(varianceValue+measureNoise)
//                mean=mean+kalmanGain*(value-mean)
//                varianceValue = varianceValue-(kalmanGain*varianceValue)
//                filtered_list.append(round(mean))
//
//
//            }
//
//            kalOutputDict[k]=filtered_list.last
//        }
//
//
//
//        return kalOutputDict
//
//    }
    
//    func kalmanFilter(inputDict:[Int: [Double]],inputValues: [Double],initVariance: Double, noise: Double, key:Int)->[Double]{
//
//            for k in inputDict.keys{
//
//                let key=k
//                var inputValues = inputDict[k]
//
//            }
//
//            var kalmanGain = 0.0
//            let processNoise = noise
//            var varianceValue = initVariance
//            let measureNoise = standardDeviation(arr: inputValues)
//            var mean = inputValues[0]
//            var filtered_list:[Double]=[]
//
//            for value in inputValues {
//                varianceValue = varianceValue+processNoise
//                kalmanGain=varianceValue/(varianceValue+measureNoise)
//                mean=mean+kalmanGain*(value-mean)
//                varianceValue = varianceValue-(kalmanGain*varianceValue)
//                filtered_list.append(round(mean))
//            }
//            return filtered_list
//
//        }
        
        func locationManager(_ manager: CLLocationManager, didRange beacons: [CLBeacon], satisfying beaconConstraint: CLBeaconIdentityConstraint){
            
            
            if(beacons.count >= 3){
                let uuid = UUID(uuidString: "00000000-0000-0000-0000-000000000001")!
                let beaconRegion = CLBeaconRegion(uuid: uuid, identifier: "57292")

               
                
                
         
               
//                for i in beacons{
//                    if(i.rssi != 0){
//
//                        let acc = round(i.accuracy*1000)/1000.0
//                        if ((bDict[Int(truncating: i.minor)]) != nil){
//                            bDict[Int(truncating: i.minor)]?.append(acc)
//                        }else{
//                            bDict[Int(truncating: i.minor)]=[acc]
//                        }
//
//
//                    }
//
//                }
                var mDict = [Int:Double]()
                for i in beacons{
                    if(i.rssi != 0){
                        
                        for b in beacons{
                            if(b.rssi != 0){
                                mDict[Int(b.minor)]=Double(b.rssi)
                            }


                        }

                        let acc = Double(i.rssi)
                        if ((bDict[Int(truncating: i.minor)]) != nil){
                            bDict[Int(truncating: i.minor)]?.append(acc)
                        }else{
                            bDict[Int(truncating: i.minor)]=[acc]
                        }


                    }

                }
                                    

                
                if(bDict[bDict.first!.key]?.count==20) {
                    let kalDict = kalmanFilter(inputDict: bDict, initVariance: 0.0625, noise: 0.4, matchDict: mDict)
                    //print("kal",kalDict)
                    for (key,dist) in kalDict{
                        let dSum = dist.reduce(0,+)
                        let aveD = round(1000*dSum/Double(dist.count))/1000
                        rssiList[key]=aveD
                    }
                    //rssiList = kalDict
                
                    numTest="ready"
                    ib1=rssiList[81] ?? 0.0
                    ib2=rssiList[186] ?? 0.0
                    ib3=rssiList[84] ?? 0.0
                    ib4=rssiList[185] ?? 0.0
                    ib5=rssiList[174] ?? 0.0
                    ib6=rssiList[194] ?? 0.0
                    ib7=rssiList[61] ?? 0.0
                    ib8=rssiList[173] ?? 0.0
                    ib9=rssiList[176] ?? 0.0
                    ib10=rssiList[184] ?? 0.0
//                    ib1=kalDict[81] ?? 0.0
//                    ib2=kalDict[186] ?? 0.0
//                    ib3=kalDict[84] ?? 0.0
//                    ib4=kalDict[185] ?? 0.0
//                    ib5=kalDict[174] ?? 0.0
//                    ib6=kalDict[194] ?? 0.0
//                    ib7=kalDict[61] ?? 0.0
//                    ib8=kalDict[173] ?? 0.0
//                    ib9=kalDict[176] ?? 0.0
//                    ib10=kalDict[184] ?? 0.0
                    
                    LM.stopMonitoring(for: beaconRegion)
                    LM.stopRangingBeacons(satisfying: beaconRegion.beaconIdentityConstraint)
                }
                
               // print(bDict)
                
                
                

//                        if(c==30){
//                            let uuid = UUID(uuidString: "00000000-0000-0000-0000-000000000002")!
//                            let beaconRegion = CLBeaconRegion(uuid: uuid, identifier: "57292")
//                            LM.stopMonitoring(for: beaconRegion)
//                            LM.stopRangingBeacons(satisfying: beaconRegion.beaconIdentityConstraint)
//
//                        }

                       
                
                        
                       
                    }

            }
        }
    

