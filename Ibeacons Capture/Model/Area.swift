//
//  Area.swift
//  Ibeacons Capture
//
//  Created by 荆海洋 on 2021-07-21.
//
	
import Foundation
import UIKit
struct Area: Identifiable{
    var id: String = UUID().uuidString
    var psX: Double
    var psY: Double
    var rssi = [Int:Double]()
    var floor: Int
    var placeName: String
    var zone : String
}
