//
//  DbManager.swift
//  Ibeacons Capture
//
//  Created by 荆海洋 on 2021-07-21.
//

import Foundation
import FirebaseFirestore

class DbManager:ObservableObject{
    let AreaRef = Firestore.firestore().collection("Area2")
    @Published var areaList = [Area]()
    
    
    func createArea(areaobj: Area, completion: @escaping (String,String) -> Void){
        var documentID = ""
        var ref: DocumentReference? = nil
//        ref = try AreaRef.addDocument(data: ["psX" : areaobj.psX,
//                                             "psY": areaobj.psY,
//                                             "rssi":["56":areaobj.rssi[56] ?? 0.0,
//                                                     "73":areaobj.rssi[73] ?? 0.0,
//                                                     "195":areaobj.rssi[195] ?? 0.0,
//                                                     "72":areaobj.rssi[72] ?? 0.0,
//                                                     "65":areaobj.rssi[65] ?? 0.0,
//                                                     "175":areaobj.rssi[175] ?? 0.0],
//
//                                             "zone":areaobj.zone,
//                                             "floor": areaobj.floor,
//                                             "placeName":areaobj.placeName
//                                            ]){
//            err in
//            if let err = err {
//                completion(documentID, err.localizedDescription)
//                print("Error adding document: \(err)")
//            } else {
//                completion(documentID, "")
//                print("Document added with ID: \(ref!.documentID)")
//            }
//        }
        let areaData:[String:Double] = ["81":areaobj.rssi[81] ?? 0.0,
                        "174":areaobj.rssi[174] ?? 0.0,
                        "84":areaobj.rssi[84] ?? 0.0,
                        "186":areaobj.rssi[186] ?? 0.0,
                        "185":areaobj.rssi[185] ?? 0.0,
                        "61":areaobj.rssi[61] ?? 0.0,
                        "173":areaobj.rssi[173] ?? 0.0,
                        "176":areaobj.rssi[176] ?? 0.0,
                        "184":areaobj.rssi[184] ?? 0.0,
                        "194":areaobj.rssi[194] ?? 0.0]
        
        ref =  AreaRef.addDocument(data: [
            
            "id": UUID().uuidString,
            "psX" : areaobj.psX,
            "psY": areaobj.psY,
            "rssi":areaData,
            "zone":areaobj.zone,
            "floor": areaobj.floor,
            "placeName":areaobj.placeName
                                             
                                            ]){
            err in
            if let err = err {
                completion(documentID, err.localizedDescription)
                print("Error adding document: \(err)")
            } else {
                completion(documentID, "")
                print("Document added with ID: \(ref!.documentID)")
            }
        }
        
    }
    

}
